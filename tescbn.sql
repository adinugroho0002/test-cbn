-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.25a - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4775
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for testcbn_db
CREATE DATABASE IF NOT EXISTS `testcbn_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `testcbn_db`;


-- Dumping structure for table testcbn_db.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `uuid_customer` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `uuid_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`uuid_customer`),
  KEY `FK_customer_user` (`uuid_user`),
  CONSTRAINT `FK_customer_user` FOREIGN KEY (`uuid_user`) REFERENCES `user` (`uuid_user`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Dumping data for table testcbn_db.customer: ~1 rows (approximately)
DELETE FROM `customer`;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`uuid_customer`, `name`, `city`, `country`, `uuid_user`) VALUES
	(1, 'test customer', 'test', 'test', 1);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


-- Dumping structure for table testcbn_db.pengajuan_kredit
CREATE TABLE IF NOT EXISTS `pengajuan_kredit` (
  `uuid_pengajuan_kredit` int(11) NOT NULL AUTO_INCREMENT,
  `uuid_customer` int(11) DEFAULT NULL,
  `penghasilan` decimal(15,0) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `uraian` varchar(100) DEFAULT NULL,
  `pemeriksa1` decimal(1,0) DEFAULT '0',
  `pemeriksa2` decimal(1,0) DEFAULT '0',
  `manager` decimal(1,0) DEFAULT '0',
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid_pengajuan_kredit`),
  KEY `FK__customer` (`uuid_customer`),
  CONSTRAINT `FK__customer` FOREIGN KEY (`uuid_customer`) REFERENCES `customer` (`uuid_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table testcbn_db.pengajuan_kredit: ~4 rows (approximately)
DELETE FROM `pengajuan_kredit`;
/*!40000 ALTER TABLE `pengajuan_kredit` DISABLE KEYS */;
INSERT INTO `pengajuan_kredit` (`uuid_pengajuan_kredit`, `uuid_customer`, `penghasilan`, `email`, `uraian`, `pemeriksa1`, `pemeriksa2`, `manager`, `tanggal`) VALUES
	(1, 1, 10000000, 'adi.adinugroho573@gmail.com', 'Permohonan Pengajuan Kredit Mobil Toyota Kijang Inova.', 0, 0, 0, '2016-10-09 07:46:53'),
	(2, 1, 12000000, 'adi.adinugroho573@gmail.com', 'Pengajuan Test', 0, 0, 0, '2016-10-09 09:29:20'),
	(3, 1, 1000000, 'adi.adinugroho573@gmail.com', 'Pengajuan Test 2', 1, 1, 1, '2016-10-09 09:29:24');
/*!40000 ALTER TABLE `pengajuan_kredit` ENABLE KEYS */;


-- Dumping structure for table testcbn_db.petugas
CREATE TABLE IF NOT EXISTS `petugas` (
  `uuid_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `jabatan` decimal(1,0) DEFAULT NULL,
  `uuid_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`uuid_petugas`),
  KEY `FK_pegawai_user` (`uuid_user`),
  CONSTRAINT `FK_pegawai_user` FOREIGN KEY (`uuid_user`) REFERENCES `user` (`uuid_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table testcbn_db.petugas: ~3 rows (approximately)
DELETE FROM `petugas`;
/*!40000 ALTER TABLE `petugas` DISABLE KEYS */;
INSERT INTO `petugas` (`uuid_petugas`, `nama`, `jabatan`, `uuid_user`) VALUES
	(1, 'petugas1', 1, 2),
	(2, 'petugas2', 2, 3),
	(3, 'manager', 3, 4);
/*!40000 ALTER TABLE `petugas` ENABLE KEYS */;


-- Dumping structure for table testcbn_db.user
CREATE TABLE IF NOT EXISTS `user` (
  `uuid_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `grup` decimal(1,0) DEFAULT NULL,
  `confirm` decimal(1,0) DEFAULT '0',
  `kirim_email` decimal(1,0) DEFAULT '0',
  PRIMARY KEY (`uuid_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table testcbn_db.user: ~4 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`uuid_user`, `username`, `password`, `grup`, `confirm`, `kirim_email`) VALUES
	(1, 'test@test', 'test', 0, 0, 0),
	(2, 'petugas1@kredit', 'test', 1, 0, 0),
	(3, 'petugas2@kredit', 'test', 2, 0, 0),
	(4, 'manager@kredit', 'test', 3, 0, 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
