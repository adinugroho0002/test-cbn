<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model','user');
	}

	public function index()
	{
		$this->load->view('v_login');		
	}

	public function cek_login()
	{
		$username = $this->input->post('user');
		$password = $this->input->post('pass');
		$user = $this->user->cek($username,$password);
		if($user)
		{
			$array = array(
				'islogin' => TRUE,
				'uuid_user' => $user->uuid_user,
				'grup' => $user->grup
			);

			if($user->grup == '0'){
				$array2 = array(
					'nama' => $user->name,
					'uuid_customer' => $user->uuid_customer
					);
				$array = array_merge($array,$array2);
			} else{				
				$array2 = array(
					'nama' => $user->nama
					);
				$array = array_merge($array,$array2);
			}
			
			$this->session->set_userdata( $array );
			redirect(base_url('home'));	
		}
		else
		{
			redirect(base_url('login'));
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */