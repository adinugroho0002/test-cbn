<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer_model', 'customer');
	}
	public function index()
	{
		if($this->session->userdata('uuid_customer')){
			redirect(base_url('home'));
		}
		$data['isi'] = 'customer';
		$this->load->view('template', $data);
	}

	public function save_api()
	{
		$data = array(
				'name' => $this->input->post('name'),
				'city' => $this->input->post('city'),				
				'country' => $this->input->post('country'),
				);
		if($this->customer->get_by_name($this->input->post('name'))){

		} else{
			$insert = $this->customer->save($data);
		}
		echo json_encode(array("status" => TRUE));
	}

}

/* End of file Customer.php */
/* Location: ./application/controllers/Customer.php */