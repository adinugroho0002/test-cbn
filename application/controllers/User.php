<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model','user');
	}

	public function customer()
	{
		if($this->session->userdata('grup') != '3'){
			redirect(base_url('home'));
		}
		$data['isi'] = 'user_customer';
		$this->load->view('template', $data);
	}

	public function petugas()
	{
		if($this->session->userdata('grup') != '3'){
			redirect(base_url('home'));
		}
		$data['isi'] = 'user_petugas';
		$this->load->view('template', $data);
	}

	public function ajax_user_customer()
	{
		$list = $this->user->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $user) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $user->name;
			$row[] = $user->username;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->user->count_all(),
						"recordsFiltered" => $this->user->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_user_petugas()
	{
		$list = $this->user->get_datatables_petugas();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $user) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $user->nama;
			$row[] = $user->username;
			switch ($user->jabatan) {
							case '1':
								$row[] = 'Pemeriksa 1';
								break;
							case '2':
								$row[] = 'Pemeriksa 2';
								break;
							case '3':
								$row[] = 'Manager';
								break;
						}			
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->user->count_all_petugas(),
						"recordsFiltered" => $this->user->count_filtered_petugas(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */