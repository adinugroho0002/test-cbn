<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pengajuan_model','pengajuan');		
		$this->load->library('PHPMailerAutoload');
	}

	public function index()
	{
		$data['isi'] = 'pengajuan_customer';
		$this->load->view('template', $data);
	}

	public function petugas()
	{
		if($this->session->userdata('uuid_customer')){
			redirect(base_url('home'));
		}
		$data['isi'] = 'pengajuan_petugas';
		$this->load->view('template', $data);
	}

	public function ajax_customer_list()
	{
		$list = $this->pengajuan->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $pengajuan) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $pengajuan->tanggal;
			$row[] = number_format($pengajuan->penghasilan,0,',','.');
			$row[] = $pengajuan->email;
			$row[] = $pengajuan->uraian;

			if($pengajuan->pemeriksa1 == '0'){
				$row[] = 'Proses';
			}

			if($pengajuan->pemeriksa1 == '1'){
				$row[5] = 'Disetujui Pemeriksa 1';
			} else if($pengajuan->pemeriksa1 == '2'){
				$row[5] = 'Ditolak';
			}

			if($pengajuan->pemeriksa2 == '1'){
				$row[5] = 'Disetujui Pemeriksa 2';
			} else if($pengajuan->pemeriksa2 == '2'){
				$row[5] = 'Ditolak';
			}

			if($pengajuan->manager == '1'){
				$row[5] = 'Disetujui';
			} else if($pengajuan->manager == '2'){
				$row[5] = 'Ditolak';
			}

			if($pengajuan->pemeriksa1 == 0){
				$row[] = '<button class="btn btn-xs btn-primary" onclick="edit_pengajuan('.$pengajuan->uuid_pengajuan_kredit.')"><i class="glyphicon glyphicon-pencil"></i> Edit</button>';
			} else{
				$row[] = '';
			}
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->pengajuan->count_all(),
						"recordsFiltered" => $this->pengajuan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->pengajuan->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
				'uuid_customer' => $this->session->userdata('uuid_customer'),
				'penghasilan' => $this->input->post('penghasilan'),
				'email' => $this->input->post('email'),
				'uraian' => $this->input->post('uraian'),
			);

		$insert = $this->pengajuan->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{		
		$data = array(				
				'penghasilan' => $this->input->post('penghasilan'),
				'email' => $this->input->post('email'),
				'uraian' => $this->input->post('uraian'),	
			);

		$this->pengajuan->update(array('uuid_pengajuan_kredit' => $this->input->post('uuid_pengajuan_kredit')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->hotel->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_petugas_list()
	{
		$list = $this->pengajuan->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $pengajuan) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $pengajuan->name;
			$row[] = $pengajuan->tanggal;
			$row[] = number_format($pengajuan->penghasilan,0,',','.');
			$row[] = $pengajuan->email;
			$row[] = $pengajuan->uraian;

			if($this->session->userdata('grup') == '1'){
				switch ($pengajuan->pemeriksa1) {
					case '0':
						$row[] = 'Menunggu';
					break;
					case '1':
					$row[] = 'Disetujui';
					break;
					case '2':
					$row[] = 'Ditolak';
					break;
				}
			}

			if($this->session->userdata('grup') == '2'){
				switch ($pengajuan->pemeriksa2) {
					case '0':
						$row[] = 'Menunggu';
					break;
					case '1':
					$row[] = 'Disetujui';
					break;
					case '2':
					$row[] = 'Ditolak';
					break;
				}
			}

			if($this->session->userdata('grup') == '3'){
				switch ($pengajuan->manager) {
					case '0':
						$row[] = 'Menunggu';
					break;
					case '1':
					$row[] = 'Disetujui';
					break;
					case '2':
					$row[] = 'Ditolak';
					break;
				}
			}

			$row[] = '<button class="btn btn-xs btn-success" onclick="setujui('.$pengajuan->uuid_pengajuan_kredit.','.$this->session->userdata('grup').')">Setujui</button>
			<button class="btn btn-xs btn-danger" onclick="tolak('.$pengajuan->uuid_pengajuan_kredit.','.$this->session->userdata('grup').')">Tolak</button>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->pengajuan->count_all(),
						"recordsFiltered" => $this->pengajuan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function setujui($id,$pemeriksa){
		switch ($pemeriksa) {
			case '1':						
				$data = array(				
						'pemeriksa1' => '1',	
					);
				break;
			case '2':						
				$data = array(				
						'pemeriksa2' => '1',	
					);
				break;
			case '3':						
				$data = array(				
						'manager' => '1',	
					);
			$this->kirim_email($id,'Selamat, pengajuan kredit Anda DISETUJUI.');
			break;
		}

		$this->pengajuan->update(array('uuid_pengajuan_kredit' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function tolak($id,$pemeriksa){
		switch ($pemeriksa) {
			case '1':						
				$data = array(				
						'pemeriksa1' => '2',	
					);
				break;
			case '2':						
				$data = array(				
						'pemeriksa2' => '2',	
					);
				break;
			case '3':						
				$data = array(				
						'manager' => '2',	
					);
				$this->kirim_email($id,'Maaf, pengajuan kredit Anda DITOLAK.');
				break;
		}

		$this->pengajuan->update(array('uuid_pengajuan_kredit' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function kirim_email($id,$pesan)
	{
		$pengajuan = $this->pengajuan->get_by_id($id);

		$mail = new PHPMailer();

		//set smtp
		$mail->isSMTP();
		$mail->Host = 'ssl://smtp.gmail.com';
		$mail->Port = '465';
		$mail->SMTPAuth = true;
		$mail->Username = 'adi.adinugroho573@gmail.com';
		$mail->Password = '';
		//set email content
		$mail->setFrom('adi.adinugroho573@gmail.com','Tes CBN');
		$mail->addAddress($pengajuan->email);
		$mail->Subject = 'Jawaban Pengajuan Kredit';
		$mail->Body = $pesan;

		if($mail->Send())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}

}

/* End of file Pengajuan.php */
/* Location: ./application/controllers/Pengajuan.php */