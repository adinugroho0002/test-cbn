<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pengajuan Kredit</a></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tabel Pengajuan Kredit Customer
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">

                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="table">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Customer</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Penghasilan Perbulan</th>
                                <th>Emaill</th>
                                <th>Uraian</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>



<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="document.getElementById('text2').value = '';">×</button>
                <h4 class="modal-title" id="myModalLabel">Mohon Tunggu..</h4>
            </div>
            <div class="modal-body">
             Proses Mengirim Email.
            </div>
            <div class="modal-footer">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


 <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script>
var save_method;
    $(document).ready(function() {

        table = $('#table').DataTable({ 

            "processing": true,
            "serverSide": true, 
            "scrollX": false,
            "autoWidth": true,
            "order": [],
            
            "ordering": false,

            "ajax": {
                "url": "<?php echo site_url('pengajuan/ajax_petugas_list')?>",
                "type": "POST"
            },

            "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
            ],

        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    function setujui(id,grup)
    {
        if(confirm('Anda yakin ingin menyetujui pengajuan ?'))
        {
            if(grup == 3){
                $('#loading').modal('show');
            }
            $.ajax({
                url : "<?php echo site_url('pengajuan/setujui/')?>"+id+"/"+grup,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    $('#loading').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error menyetujui data');
                }
            });

        }
    }

    function tolak(id,grup)
    {
        if(confirm('Anda yakin ingin menolak pengajuan ?'))
        {
            if(grup == 3){
                $('#loading').modal('show');
            }
            $.ajax({
                url : "<?php echo site_url('pengajuan/tolak/')?>"+id+"/"+grup,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    $('#loading').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error menolak data');
                }
            });

        }
    }
</script>