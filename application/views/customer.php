<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Data Customer Dari API</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
        <?php if ($this->session->flashdata('info')): ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('info'); ?>
            </div>
        <?php endif; ?>
            <div class="panel panel-default">
                <div class="panel-heading judul">
                    Tabel Data Absensi Pegawai
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="col-md-4">
                        <button class="btn btn-default" onclick="save_data()"><i class="glyphicon glyphicon-send"></i> Simpan ke Database</button>
                    </div>
                    <br>
                    <br>
                    <br>
                 
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="table">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Country</th>
                            </tr>
                            </thead>
                            <tbody>                            
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

<!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script>

    $(document).ready(function() {
        $.ajax({
            url : "http://www.w3schools.com/angular/customers.php",
            type: "GET",
            dataType: "JSON",
            success: function(customer)
            {
                var no = 1;
              $.each(customer.records, function(index, element){
                var tablerow = "<tr>";
                tablerow += "<td>"+(no++)+"</td>";
                tablerow += "<td>"+element.Name+"</td>";
                tablerow += "<td>"+element.City+"</td>";
                tablerow += "<td>"+element.Country+"</td>";
                tablerow += "</tr>";
                $(table).append(tablerow);               
              })
            }
        })

    });

function save_data(){
    $.ajax({
            url : "http://www.w3schools.com/angular/customers.php",
            type: "GET",
            dataType: "JSON",
            success: function(customer)
            {
              $.each(customer.records, function(index, element){
                var tablerow = "<tr>";
                tablerow += "<td>"+element.Name+"</td>";
                tablerow += "<td>"+element.City+"</td>";
                tablerow += "<td>"+element.Country+"</td>";
                tablerow += "</tr>";
                $(table).append(tablerow);
                $.ajax({
                url : "<?php echo site_url('customer/save_api')?>",
                type: "POST",
                data: {name:element.Name,city:element.City,country:element.Country},
                dataType: "JSON",
                success: function(data)
                {

                    if(data.status) //if success close modal and reload ajax table
                    {
                        $('#modal_form').modal('hide');
                        //reload_table();                    
                        $('#myModal').modal('hide');
                    }
                    //alert('Data Berhasil Disimpan');


                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    //alert('Error');

                }
                });
                              
              })
            alert('Data Berhasil Disimpan.');  
            }
        })
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
</script>