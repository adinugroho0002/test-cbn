<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
        <div id="dialogboxhead"></div>
        <div id="dialogboxbody"></div>
        <div id="dialogboxfoot"></div>
    </div>
</div>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li style="" class="sidebar-search">
                <div class="input-group custom-search-form">
                    <span style="font-size: 23px; margin-left: 10px;">
                    <script language="javascript">
                        var tanggallengkap = new String();
                        var namahari = ("Minggu Senin Selasa Rabu Kamis Jumat Sabtu");
                        namahari = namahari.split(" ");
                        var namabulan = ("Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember");
                        namabulan = namabulan.split(" ");
                        var tgl = new Date();
                        var hari = tgl.getDay();
                        var tanggal = tgl.getDate();
                        var bulan = tgl.getMonth();
                        var tahun = tgl.getFullYear();
                        tanggallengkap = tanggal + " " + namabulan[bulan] + " " + tahun;
                        document.write(tanggallengkap);
                    </script>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            
            <li>
                <a href="<?php echo base_url(); ?>home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="<?php if($this->session->userdata('uuid_customer')) {echo base_url('pengajuan');} else{echo base_url('pengajuan/petugas');} ?>"><i class="fa fa-table fa-fw"></i> Pengajuan Kredit</a>
            </li>
            <?php if ($this->session->userdata('grup') == '2' or $this->session->userdata('grup') == '1' or $this->session->userdata('grup') == '3'){ ?>
            <li>
                <a href="<?php echo base_url(); ?>customer"><i class="fa fa-table fa-fw"></i> Data Customer</a>
            </li>            
            <?php } ?>
            <?php if ($this->session->userdata('grup') == '3'){ ?>
            <li>
                <a href="<?php echo base_url(); ?>user/customer"><i class="fa fa-table fa-fw"></i> User Customer</a>
            </li>            
            <li>
                <a href="<?php echo base_url(); ?>user/petugas"><i class="fa fa-table fa-fw"></i> User Petugas</a>
            </li>
            <?php } ?>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>