<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pengajuan Kredit</a></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
        <?php if ($this->session->flashdata('info')): ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('info'); ?>
            </div>
        <?php endif; ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tabel Pengajuan Kredit Customer
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <button id="tombol" style="margin-bottom: 15px;" class="btn btn-success" onclick="add_pengajuan()">
                        <i class="fa fa-plus"></i> Pengajuan Kredit Baru
                    </button>

                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="table">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Penghasilan Perbulan</th>
                                <th>Emaill</th>
                                <th>Uraian</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="document.getElementById('text2').value = '';">×</button>
                <h4 class="modal-title" id="myModalLabel">Pengajuan Baru</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="form" action="<?php echo base_url(); ?>master/agama/tambah">
                    <input type="hidden" name="uuid_pengajuan_kredit" value="">
                    <div class="form-group">
                        <label>Penghasilan Per Bulan</label>
                        <input type="number" id="text2" name="penghasilan" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" id="text2" name="email" class="form-control">
                    </div>                    
                    <div class="form-group">
                        <label>Uraian</label>
                        <input type="text" id="text2" name="uraian" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button id="btnSave" class="btn btn-primary" onclick="save_pengajuan()">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

 <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script>
var save_method;
    $(document).ready(function() {

        table = $('#table').DataTable({ 

            "processing": true,
            "serverSide": true, 
            "scrollX": false,
            "autoWidth": true,
            "order": [],
            
            "ordering": false,

            "ajax": {
                "url": "<?php echo site_url('pengajuan/ajax_customer_list')?>",
                "type": "POST"
            },

            "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
            ],

        });
    });

    function add_pengajuan(){
        save_method = 'add';
        $('#form')[0].reset();
        $('.modal-title').text('Tambah Data Pengajuan');
        $('#myModal').modal('show');
    }

    function edit_pengajuan(id)
    {
        save_method = 'update';
        $('#form')[0].reset();

        $.ajax({
            url : "<?php echo site_url('pengajuan/ajax_edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="uuid_pengajuan_kredit"]').val(data.uuid_pengajuan_kredit);
                $('[name="penghasilan"]').val(data.penghasilan);
                $('[name="email"]').val(data.email);
                $('[name="uraian"]').val(data.uraian);                
                $('.modal-title').text('Ubah Data Pengajuan');
                $('#myModal').modal('show');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function save_pengajuan(){
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('pengajuan/ajax_add')?>";
        } else {
            url = "<?php echo site_url('pengajuan/ajax_update')?>";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();                    
                    $('#myModal').modal('hide');
                }

                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Nama, Sistem, Maskapai, atau Staff Issued belum diisi.');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 

            }
        });
    }

    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
    }
</script>