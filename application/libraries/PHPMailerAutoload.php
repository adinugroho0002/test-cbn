<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PHPMailerAutoload {

	public function PHPMailerAutoload()
	{
		require_once('PHPMailer/PHPMailerAutoload.php');
	}

}

/* End of file PHPMailer.php */
/* Location: ./application/libraries/PHPMailer.php */